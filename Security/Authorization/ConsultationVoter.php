<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Chill\HealthBundle\Security\Authorization;

use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Security\ProvideRoleInterface;
use Chill\HealthBundle\Entity\Consultation;

/**
 * Check the Access Model for consultation.
 * 
 * This class also check the consultation state :
 * 
 * - if the consultation is closed => the consultation is not allowed to be 
 *   updated ;
 * - if the consultation is draft => we check that the user has the required
 *   access
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ConsultationVoter extends AbstractChillVoter implements ProvideRoleInterface
{
    const CREATE = 'CHILL_HEALTH_CONSULTATION_CREATE';
    const SEE    = 'CHILL_HEALTH_CONSULTATION_SEE';
    const UPDATE = 'CHILL_HEALTH_CONSULTATION_UPDATE';
    
    /**
     *
     * @var AuthorizationHelper
     */
    protected $helper;
    
    public function __construct(AuthorizationHelper $helper)
    {
        $this->helper = $helper;
    }
    
    protected function getSupportedAttributes()
    {
        return [self::CREATE, self::SEE, self::UPDATE];
    }

    protected function getSupportedClasses()
    {
        return [Consultation::class];
    }

    protected function isGranted($attribute, $consultation, $user = null)
    {
        if (! $user instanceof \Chill\MainBundle\Entity\User) {
            return false;
        }
        
        if ($attribute === self::UPDATE) {
            if ($consultation->getState() === Consultation::STATE_CLOSED) {
                return false;
            }
        }
        
        return $this->helper->userHasAccess($user, $consultation, $attribute);
    }

    public function getRoles()
    {
        $this->getSupportedAttributes();
    }

    public function getRolesWithoutScope()
    {
        return array();
    }
}
