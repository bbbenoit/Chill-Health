<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\HealthBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Chill\HealthBundle\Entity\Code;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class LoadCodes extends AbstractFixture implements OrderedFixtureInterface, 
    ContainerAwareInterface
{
    use \Symfony\Component\DependencyInjection\ContainerAwareTrait;
    
    public function getOrder()
    {
        return 56001;
    }
    
    protected $substances = array(
        array(
            'substance',
            '387286002',
            '2.16.840.1.113883.6.96',
            //\array_combine(['fr', 'nl', 'en'], \array_fill('Methadone (substance)')),
            'Methadone (substance)',
            []
        ),
        array(
            'substance',
            '387264003',
            '2.16.840.1.113883.6.96',
            'Diazepiam (substance)',
            []
        ),
        array(
            'substance',
            '387173000',
            '2.16.840.1.113883.6.96',
            'Buprenorphine (substance)',
            []
        )
    );

    public function load(ObjectManager $manager)
    {
        foreach ($this->substances as list($type, $code, $codeSystem, $name, 
            $data)) {
            $code = (new Code())
                ->setType($type)
                ->setCode($code)
                ->setCodeSystem($codeSystem)
                ->setDisplayName(\array_combine(
                        ['fr', 'nl', 'en'],
                        \array_fill(0, 3, $name)
                    )
                );
            $manager->persist($code);
        }
        
        // load known codes
        // 
        // route of administration
        $codeProvider = $this->container->get('chill_health.'
            . 'code_provider_route_of_administration');
        foreach($codeProvider->getCodes() as $code) {
            $manager->persist($code);
        }
        
        $manager->flush();
    }
}
