<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\HealthBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Chill\HealthBundle\Entity\Code;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Chill\HealthBundle\Entity\Medication;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Chill\HealthBundle\DataFixtures\ORM\LoadConsultations;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class LoadMedications extends AbstractFixture implements OrderedFixtureInterface,
    ContainerAwareInterface
{
    use \Symfony\Component\DependencyInjection\ContainerAwareTrait;
    
    public function getOrder()
    {
        return 56003;
    }

    public function load(ObjectManager $manager)
    {
        $nbMedications = [0, 1, 1, 2];
        
        foreach (LoadConsultations::$refs as $ref) {
            $consultation = $this->getReference($ref);
            $nbMedication = $nbMedications[\array_rand($nbMedications)];
            
            for ($i = 0; $i < $nbMedication; $i++) {
                $medication = (new Medication());
                // TODO add information...
            }
            
        }
    }
}
