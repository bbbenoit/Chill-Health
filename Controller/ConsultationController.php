<?php

namespace Chill\HealthBundle\Controller;

use Chill\HealthBundle\Form\CloseConsultationType;
use Chill\HealthBundle\Entity\Consultation;
use Chill\HealthBundle\Security\Authorization\ConsultationVoter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Chill\HealthBundle\Entity\Medication;
use Chill\HealthBundle\Form\MedicationType;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ConsultationController extends Controller
{
    /**
     * 
     * @param int $id personId
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws type
     */
    public function listAction($person_id)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $this->get('chill.person.repository.person')
            ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException("The person is not found");
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        /* @var $authorizationHelper \Chill\MainBundle\Security\Authorization\AuthorizationHelper */
        $authorizationHelper = $this->get('chill.main.security.'
            . 'authorization.helper');
        
        $circles = $authorizationHelper->getReachableCircles(
            $this->getUser(), 
            new Role(ConsultationVoter::SEE), 
            $person->getCenter()
            );
        
        $consultations = $this->getDoctrine()->getManager()
            ->createQuery('SELECT c FROM ChillHealthBundle:Consultation c '
                . 'WHERE c.patient = :person AND c.circle IN(:circles) '
                . 'ORDER BY c.date DESC')
            ->setParameter('person', $person)
            ->setParameter('circles', $circles)
            ->getResult();
        
        return $this->render('ChillHealthBundle:Consultation:list.html.twig', array(
                'person' => $person,
                'consultations' => $consultations
            ));    
    }

    public function newAction($id)
    {
        return $this->render('ChillHealthBundle:Consultation:new.html.twig', array(
                // ...
            ));    
        
    }
    
    public function showAction($consultation_id, Request $request)
    {   
        /* @var $consultation \Chill\HealthBundle\Entity\Consultation */
        $consultation = $this->get('chill_health.repository_consultation')
            ->find($consultation_id);
        
        if ($consultation === null) {
            throw $this->createNotFoundException("consultation not found");
        }
        
        $this->denyAccessUnlessGranted(ConsultationVoter::SEE, $consultation);
        
        $person = $consultation->getPatient();
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        // embed the form if required
        $params = array();
        if ($request->query->has('action')) {
            switch ($request->query->get('action')) {
                case 'medication_add':
                    $params['form_medication'] = $this
                        ->createMedicationForm($consultation)
                        ->createView();
                    break;
                case 'medication_highlight':
                    $params['medication_highlight'][] = $request->query
                        ->getInt('medication_id', null);
                    break;
                case 'suggest_close':
                    $params['form_close'] = $this
                        ->createConsultationCloseForm($consultation)
                        ->createView();
                    break;
                default:
                    throw new \RuntimeException(sprintf("The action '%s' "
                        . "is unknown", $request->query->get('action')));
            }
        }
        
        return $this->render("ChillHealthBundle:Consultation:show.html.twig",
            \array_merge(
                array(
                    'person' => $person,
                    'consultation' => $consultation
                ), 
                $params)
            );
    }
    
    public function closeAction($consultation_id, Request $request)
    {
        /* @var $consultation \Chill\HealthBundle\Entity\Consultation */
        $consultation = $this->get('chill_health.repository_consultation')
            ->find($consultation_id);
        
        if ($consultation === null) {
            throw $this->createNotFoundException("consultation not found");
        }
        
        $this->denyAccessUnlessGranted(ConsultationVoter::SEE, $consultation);
        
        $person = $consultation->getPatient();
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        $form = $this->createConsultationCloseForm($consultation);
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $consultation->setState(Consultation::STATE_CLOSED);
            
            $this->getDoctrine()->getManager()
                ->flush();
            
            $this->addFlash(('success'), $this->get('translator')
                ->trans('The consultation is closed.'));
            
            return $this->redirectToRoute('chill_health_consultation_show', 
                [
                    'consultation_id' => $consultation->getId()
                ]);
        }
        
        $this->addFlash('error', $this->get('translator')
            ->trans('The consultation could not be closed.'));
        
        return $this->redirectToRoute('chill_health_consultation_show', 
            [
                'consultation_id' => $consultation->getId(),
                'action' => 'suggest_close'
            ]);
    }
    
    /**
     * 
     * @param \Chill\HealthBundle\Controller\Consultation $consultation
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createMedicationForm(Consultation $consultation)
    {
        return $this->createForm(
                MedicationType::class, 
                (new Medication())->setConsultation($consultation)
                ,
                [
                    'action' => $this
                        ->generateUrl('chill_health_medication_create'),
                    'method' => 'POST'
                ]
            )
            ->add('submit', SubmitType::class)
            ;
    }
    
    protected function createConsultationCloseForm(Consultation $consultation)
    {
        $builder = $this->createFormBuilder(
            [
                'consultation' => $consultation->getId(),
                'submit' => 'close'
            ], 
            [
                'action' => $this->generateUrl(
                    'chill_health_consultation_close',
                    [
                        'consultation_id' => $consultation->getId()
                    ]),
                'method' => 'POST'
        ])
            ->add('consultation', HiddenType::class)
            ->add('submit', SubmitType::class);
        
        return $builder->getForm();
    }

}
