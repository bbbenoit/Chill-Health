<?php

namespace Chill\HealthBundle\Entity;

use Chill\MainBundle\Entity\HasScopeInterface;
use Chill\MainBundle\Entity\HasCenterInterface;

/**
 * Consultation
 * 
 * The state of the consultation can be :
 * 
 * - draft (the consultation may be updated) ;
 * - closed (the consultation cannot be update);
 * 
 * The fact that the consultation can be updated or not, depending on his state,
 * is handled by ConsultationVoter
 */
class Consultation implements HasScopeInterface, HasCenterInterface
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var \Chill\PersonBundle\Entity\Person
     */
    private $patient;

    /**
     * @var \Chill\MainBundle\Entity\User
     */
    private $author;

    /**
     * @var \Chill\MainBundle\Entity\Scope
     */
    private $circle;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $medications;
    
    /**
     * The state of the consultation. 
     * 
     * May be "draft" or "closed"
     *
     * @var string
     */
    private $state = self::STATE_DRAFT;
    
    /**
     * The consultation is draft an can be updated
     */
    const STATE_DRAFT = 'draft';
    
    /**
     * The consultation is closed and cannot be update
     */
    const STATE_CLOSED = 'closed';
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->medications = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Consultation
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set patient
     *
     * @param \Chill\PersonBundle\Entity\Person $patient
     *
     * @return Consultation
     */
    public function setPatient(\Chill\PersonBundle\Entity\Person $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return \Chill\PersonBundle\Entity\Person
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set author
     *
     * @param \Chill\MainBundle\Entity\User $author
     *
     * @return Consultation
     */
    public function setAuthor(\Chill\MainBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Chill\MainBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set circle
     *
     * @param \Chill\MainBundle\Entity\Scope $circle
     *
     * @return Consultation
     */
    public function setCircle(\Chill\MainBundle\Entity\Scope $circle = null)
    {
        $this->circle = $circle;

        return $this;
    }

    /**
     * Get circle
     *
     * @return \Chill\MainBundle\Entity\Scope
     */
    public function getCircle()
    {
        return $this->circle;
    }
    
    /**
     * alias for self::getCircle()
     * 
     * @return \Chill\MainBundle\Entity\Scope
     */
    public function getScope()
    {
        return $this->getCircle();
    }

        /**
     * Add medication
     *
     * @param \Chill\HealthBundle\Entity\Medication $medication
     *
     * @return Consultation
     */
    public function addMedication(\Chill\HealthBundle\Entity\Medication $medication)
    {
        $this->medications[] = $medication;

        return $this;
    }

    /**
     * Remove medication
     *
     * @param \Chill\HealthBundle\Entity\Medication $medication
     */
    public function removeMedication(\Chill\HealthBundle\Entity\Medication $medication)
    {
        $this->medications->removeElement($medication);
    }

    /**
     * Get medications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedications()
    {
        return $this->medications;
    }
    
    public function getCenter()
    {
        return $this->getPatient()->getCenter();
    }
    
    public function getState()
    {
        return $this->state;
    }

    public function setState($state)
    {
        $this->state = $state;
        
        return $this;
    }



}
