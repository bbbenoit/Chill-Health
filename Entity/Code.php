<?php

namespace Chill\HealthBundle\Entity;

/**
 * Code
 */
class Code
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var array
     */
    private $displayName;

    /**
     * @var string
     */
    private $codeSystem;
    
    /**
     * @var array
     */
    private $data = array();


    /**
     * Set type
     *
     * @param string $type
     *
     * @return Code
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Code
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set displayName
     *
     * @param array $displayName
     *
     * @return Code
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return array
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set codeSystem
     *
     * @param string $codeSystem
     *
     * @return Code
     */
    public function setCodeSystem($codeSystem)
    {
        $this->codeSystem = $codeSystem;

        return $this;
    }

    /**
     * Get codeSystem
     *
     * @return string
     */
    public function getCodeSystem()
    {
        return $this->codeSystem;
    }

    /**
     * Set data
     *
     * @param array $data
     *
     * @return Code
     */
    public function setData(array $data = array())
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}
