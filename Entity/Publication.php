<?php

namespace Chill\HealthBundle\Entity;

/**
 * Publication
 */
class Publication
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $publication_type;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $data;

    /**
     * @var \Chill\PersonBundle\Entity\Person
     */
    private $patient;

    /**
     * @var \Chill\MainBundle\Entity\User
     */
    private $author;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $medications;
    
    /**
     * @var \Chill\MainBundle\Entity\Scope
     */
    private $circle;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->medications = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set publicationType
     *
     * @param string $publicationType
     *
     * @return Publication
     */
    public function setPublicationType($publicationType)
    {
        $this->publication_type = $publicationType;

        return $this;
    }

    /**
     * Get publicationType
     *
     * @return string
     */
    public function getPublicationType()
    {
        return $this->publication_type;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Publication
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return Publication
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set patient
     *
     * @param \Chill\PersonBundle\Entity\Person $patient
     *
     * @return Publication
     */
    public function setPatient(\Chill\PersonBundle\Entity\Person $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return \Chill\PersonBundle\Entity\Person
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * Set author
     *
     * @param \Chill\MainBundle\Entity\User $author
     *
     * @return Publication
     */
    public function setAuthor(\Chill\MainBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Chill\MainBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Add medication
     *
     * @param \Chill\HealthBundle\Entity\Medication $medication
     *
     * @return Publication
     */
    public function addMedication(\Chill\HealthBundle\Entity\Medication $medication)
    {
        $this->medications[] = $medication;

        return $this;
    }

    /**
     * Remove medication
     *
     * @param \Chill\HealthBundle\Entity\Medication $medication
     */
    public function removeMedication(\Chill\HealthBundle\Entity\Medication $medication)
    {
        $this->medications->removeElement($medication);
    }

    /**
     * Get medications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedications()
    {
        return $this->medications;
    }
    
    /**
     * Set circle
     *
     * @param \Chill\MainBundle\Entity\Scope $circle
     *
     * @return Consultation
     */
    public function setCircle(\Chill\MainBundle\Entity\Scope $circle = null)
    {
        $this->circle = $circle;

        return $this;
    }

    /**
     * Get circle
     *
     * @return \Chill\MainBundle\Entity\Scope
     */
    public function getCircle()
    {
        return $this->circle;
    }
    
    /**
     * alias for self::getCircle()
     * 
     * @return \Chill\MainBundle\Entity\Scope
     */
    public function getScope()
    {
        return $this->getCircle();
    }
}

