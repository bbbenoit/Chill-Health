<?php

namespace Chill\HealthBundle\Entity;

/**
 * Medication
 */
class Medication
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $generator;

    /**
     * @var \DateTime
     */
    private $dateFrom;

    /**
     * @var string
     */
    private $substance_string = '';

    /**
     * @var string
     */
    private $doseQuantityValue;

    /**
     * @var array
     */
    private $doseQuantityUnit;

    /**
     * @var string
     */
    private $administrationUnitCode;

    /**
     * @var array
     */
    private $administrationFrequency;

    /**
     * @var Code
     */
    private $routeOfAdministration;

    /**
     * @var \DateTime
     */
    private $dateTo;

    /**
     * @var string
     */
    private $modeDelivrance = '';

    /**
     * @var boolean
     */
    private $neverPublish = false;

    /**
     * @var Consultation
     */
    private $consultation;
    
    /**
     * @var \Chill\HealthBundle\Entity\Code
     */
    private $substance;

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Medication
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set generator
     *
     * @param string $generator
     *
     * @return Medication
     */
    public function setGenerator($generator)
    {
        $this->generator = $generator;

        return $this;
    }

    /**
     * Get generator
     *
     * @return string
     */
    public function getGenerator()
    {
        return $this->generator;
    }

    /**
     * Set dateFrom
     *
     * @param \DateTime $dateFrom
     *
     * @return Medication
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    /**
     * Get dateFrom
     *
     * @return \DateTime
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Set substanceString
     *
     * @param string $substanceString
     *
     * @return Medication
     */
    public function setSubstanceString($substanceString)
    {
        $this->substance_string = $substanceString;

        return $this;
    }

    /**
     * Get substanceString
     *
     * @return string
     */
    public function getSubstanceString()
    {
        return $this->substance_string;
    }

    /**
     * Set doseQuantityValue
     *
     * @param string $doseQuantityValue
     *
     * @return Medication
     */
    public function setDoseQuantityValue($doseQuantityValue)
    {
        $this->doseQuantityValue = $doseQuantityValue;

        return $this;
    }

    /**
     * Get doseQuantityValue
     *
     * @return string
     */
    public function getDoseQuantityValue()
    {
        return $this->doseQuantityValue;
    }

    /**
     * Set doseQuantityUnit
     *
     * @param array $doseQuantityUnit
     *
     * @return Medication
     */
    public function setDoseQuantityUnit($doseQuantityUnit)
    {
        $this->doseQuantityUnit = $doseQuantityUnit;

        return $this;
    }

    /**
     * Get doseQuantityUnit
     *
     * @return array
     */
    public function getDoseQuantityUnit()
    {
        return $this->doseQuantityUnit;
    }

    /**
     * Set administrationUnitCode
     *
     * @param string $administrationUnitCode
     *
     * @return Medication
     */
    public function setAdministrationUnitCode($administrationUnitCode)
    {
        $this->administrationUnitCode = $administrationUnitCode;

        return $this;
    }

    /**
     * Get administrationUnitCode
     *
     * @return string
     */
    public function getAdministrationUnitCode()
    {
        return $this->administrationUnitCode;
    }

    /**
     * Set administrationFrequency
     *
     * @param array $administrationFrequency
     *
     * @return Medication
     */
    public function setAdministrationFrequency($administrationFrequency)
    {
        $this->administrationFrequency = $administrationFrequency;

        return $this;
    }

    /**
     * Get administrationFrequency
     *
     * @return array
     */
    public function getAdministrationFrequency()
    {
        return $this->administrationFrequency;
    }

    /**
     * Set routeOfAdministration
     *
     * @param Code $routeOfAdministration
     *
     * @return Medication
     */
    public function setRouteOfAdministration(Code $routeOfAdministration)
    {
        $this->routeOfAdministration = $routeOfAdministration;

        return $this;
    }

    /**
     * Get routeOfAdministration
     *
     * @return Code
     */
    public function getRouteOfAdministration()
    {
        return $this->routeOfAdministration;
    }

    /**
     * Set dateTo
     *
     * @param \DateTime $dateTo
     *
     * @return Medication
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;

        return $this;
    }

    /**
     * Get dateTo
     *
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Set modeDelivrance
     *
     * @param string $modeDelivrance
     *
     * @return Medication
     */
    public function setModeDelivrance($modeDelivrance)
    {
        $this->modeDelivrance = $modeDelivrance;

        return $this;
    }

    /**
     * Get modeDelivrance
     *
     * @return string
     */
    public function getModeDelivrance()
    {
        return $this->modeDelivrance;
    }

    /**
     * Set neverPublish
     *
     * @param boolean $neverPublish
     *
     * @return Medication
     */
    public function setNeverPublish($neverPublish)
    {
        $this->neverPublish = $neverPublish;

        return $this;
    }

    /**
     * Get neverPublish
     *
     * @return boolean
     */
    public function getNeverPublish()
    {
        return $this->neverPublish;
    }

    /**
     * Add consultation
     *
     * @param \Chill\HealthBundle\Entity\Consultation $consultation
     *
     * @return Medication
     */
    public function setConsultation(\Chill\HealthBundle\Entity\Consultation $consultation)
    {
        $this->consultation = $consultation;

        return $this;
    }

    /**
     * Get consultation
     *
     * @return Consultation
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * Set substance
     *
     * @param \Chill\HealthBundle\Entity\Code $substance
     *
     * @return Medication
     */
    public function setSubstance(\Chill\HealthBundle\Entity\Code $substance = null)
    {
        $this->substance = $substance;

        return $this;
    }

    /**
     * Get substance
     *
     * @return \Chill\HealthBundle\Entity\Code
     */
    public function getSubstance()
    {
        return $this->substance;
    }
}
