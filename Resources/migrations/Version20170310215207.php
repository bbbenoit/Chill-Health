<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Add a table for entities : 
 * 
 * - ChillHealthBundle:Code
 * - ChillHealthBundle:Medication
 * - ChillHealthBundle:Consultation
 */
class Version20170310215207 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection
            ->getDatabasePlatform()->getName() !== 'postgresql', 
            'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE chill_health_code_id_seq '
            . 'INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_health_medication_id_seq '
            . 'INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_health_consultation_id_seq '
            . 'INCREMENT BY 1 MINVALUE 1 START 1');

        $this->addSql('CREATE TABLE chill_health_code '
            . '(id INT NOT NULL, '
            . 'code VARCHAR(255) NOT NULL, '
            . 'displayName JSON NOT NULL, '
            . 'codeSystem VARCHAR(255) NOT NULL, '
            . 'code_type VARCHAR(30) NOT NULL,'
            . 'data JSONB NOT NULL, '
            . 'PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_chill_health_code_type '
            . 'ON chill_health_code (code_type)');
        $this->addSql('CREATE TABLE chill_health_medication ('
            . 'id INT NOT NULL, '
            . 'consultation_id INT NOT NULL, ' 
            . 'substance_id INT DEFAULT NULL, '
            . 'dateFrom TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, '
            . 'substance_string VARCHAR(255) DEFAULT NULL, '
            . 'doseQuantityValue NUMERIC(10, 0) NOT NULL, '
            . 'doseQuantityUnit VARCHAR(40) NOT NULL, '
            . 'administrationUnitCode VARCHAR(20) DEFAULT \'\', '
            . 'administrationFrequency VARCHAR(200) NOT NULL, '
            . 'routeOfAdministration_id INT DEFAULT NULL, '
            . 'dateTo TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, '
            . 'modeDelivrance TEXT DEFAULT \'\', '
            . 'neverPublish BOOLEAN NOT NULL, '
            . 'PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B9EE6CA7C707E018 '
            . 'ON chill_health_medication (substance_id)');
        $this->addSql('CREATE TABLE chill_health_consultation '
            . '(id INT NOT NULL, patient_id INT DEFAULT NULL, '
            . 'author_id INT DEFAULT NULL, '
            . 'circle_id INT DEFAULT NULL, '
            . 'date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, '
            . 'state VARCHAR(10) NOT NULL, '
            . 'PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE chill_health_medication '
            . 'ADD CONSTRAINT FK_B9EE6CA7B75CB33 '
            . 'FOREIGN KEY (routeOfAdministration_id) '
            . 'REFERENCES chill_health_code (id) '
            . 'NOT DEFERRABLE INITIALLY IMMEDIATE;');
        $this->addSql('CREATE INDEX IDX_B9EE6CA7B75CB33 ON '
            . 'chill_health_medication (routeOfAdministration_id)');
        $this->addSql('CREATE INDEX IDX_E08290716B899279 ON '
            . 'chill_health_consultation (patient_id)');
        $this->addSql('CREATE INDEX IDX_E0829071F675F31B ON '
            . 'chill_health_consultation (author_id)');
        $this->addSql('CREATE INDEX IDX_E082907170EE2FF6 ON '
            . 'chill_health_consultation (circle_id)');
        $this->addSql('ALTER TABLE chill_health_medication '
            . 'ADD CONSTRAINT FK_B9EE6CA7C707E018 FOREIGN KEY (substance_id) '
            . 'REFERENCES chill_health_code (id) '
            . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_health_medication '
            . 'ADD CONSTRAINT FK_B9EE6CA7C707E012 FOREIGN KEY (consultation_id) '
            . 'REFERENCES chill_health_consultation (id) '
            . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_health_consultation '
            . 'ADD CONSTRAINT FK_B9EE6CA762FF6CDF FOREIGN KEY (patient_id) '
            . 'REFERENCES chill_person_person (id) '
            . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_B9EE6CA762FF6CDF '
            . 'ON chill_health_medication (consultation_id)');
        $this->addSql('ALTER TABLE chill_health_consultation '
            . 'ADD CONSTRAINT FK_E0829071F675F31B FOREIGN KEY (author_id) '
            . 'REFERENCES users (id) '
            . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_health_consultation '
            . 'ADD CONSTRAINT FK_E082907170EE2FF6 FOREIGN KEY (circle_id) '
            . 'REFERENCES scopes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        
        
        // publications
        $this->addSql('CREATE SEQUENCE chill_health_publication_id_seq '
            . 'INCREMENT BY 1 MINVALUE 1 START 1;');
        $this->addSql('CREATE TABLE chill_health_publication '
            . '(id INT NOT NULL, '
            . 'patient_id INT DEFAULT NULL, '
            . 'author_id INT DEFAULT NULL, '
            . 'circle_id INT DEFAULT NULL, '
            . 'publication_type VARCHAR(100) NOT NULL, '
            . 'date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, '
            . 'data TEXT NOT NULL, '
            . 'PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE chill_health_publication ADD CONSTRAINT '
            . 'FK_B76820396B899279 '
            . 'FOREIGN KEY (patient_id) REFERENCES chill_person_person (id) '
            . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_health_publication ADD CONSTRAINT '
            . 'FK_B7682039F675F31B '
            . 'FOREIGN KEY (author_id) REFERENCES users (id) '
            . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_health_publication ADD CONSTRAINT '
            . 'FK_B768203970EE2FF6 '
            . 'FOREIGN KEY (circle_id) REFERENCES scopes (id) '
            . 'NOT DEFERRABLE INITIALLY IMMEDIATE;');
        $this->addSql('CREATE INDEX IDX_B76820396B899279 ON '
            . 'chill_health_publication (patient_id)');
        $this->addSql('CREATE INDEX IDX_B7682039F675F31B ON '
            . 'chill_health_publication (author_id)');
        $this->addSql('CREATE INDEX IDX_B768203970EE2FF6 ON '
            . 'chill_health_publication (circle_id);');
        $this->addSql('CREATE INDEX idx_publication_type ON '
            . 'chill_health_publication (publication_type);');
        
        // link between publications and medications
        $this->addSql('CREATE TABLE chill_health_publication_medication '
            . '(publication_id INT NOT NULL, '
            . 'medication_id INT NOT NULL, '
            . 'PRIMARY KEY(publication_id, medication_id))');
        $this->addSql('CREATE INDEX IDX_1E57214338B217A7 ON '
            . 'chill_health_publication_medication (publication_id)');
        $this->addSql('CREATE INDEX IDX_1E5721432C4DE6DA ON '
            . 'chill_health_publication_medication (medication_id)');
        $this->addSql('ALTER TABLE chill_health_publication_medication '
            . 'ADD CONSTRAINT FK_1E57214338B217A7 '
            . 'FOREIGN KEY (publication_id) '
            . 'REFERENCES chill_health_publication (id) '
            . 'ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_health_publication_medication '
            . 'ADD CONSTRAINT FK_1E5721432C4DE6DA '
            . 'FOREIGN KEY (medication_id) '
            . 'REFERENCES chill_health_medication (id) '
            . 'ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection
            ->getDatabasePlatform()->getName() !== 'postgresql', 
            'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chill_health_medication '
            . 'DROP CONSTRAINT FK_B9EE6CA7C707E018');
        $this->addSql('DROP SEQUENCE chill_health_code_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_health_medication_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_health_consultation_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_health_publication_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_health_medication CASCADE');
        $this->addSql('DROP TABLE chill_health_consultation CASCADE');
        $this->addSql('DROP TABLE chill_health_code CASCADE');
        $this->addSql('DROP TABLE chill_health_publication_medication CASCADE');
        $this->addSql('DROP TABLE chill_health_publication CASCADE');
    }
}
