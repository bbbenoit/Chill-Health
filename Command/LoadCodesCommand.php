<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\HealthBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Chill\HealthBundle\Entity\Code;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Load codes 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class LoadCodesCommand extends ContainerAwareCommand
{
    const SERVICE_NAME = 'service_name';
    
    protected function configure()
    {
        $this
            ->setName('chill:load:medical-code')
            ->setDescription("Load medical code inside database if not present")
            ->addArgument(self::SERVICE_NAME)
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $service = $this->getService($input, $output);
        /* @var $em \Doctrine\ORM\EntityManagerInterface */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        /* @var $codeRepository \Doctrine\ORM\EntityRepository */
        $codeRepository = $em->getRepository(Code::class);
        
        $i = 0;
        foreach ($service->getCodes() as $code) {
            $output->writeln(sprintf("Examining new code '%s'", $code->getCode()),
                OutputInterface::VERBOSITY_DEBUG);
            
            $existingCode = $codeRepository->findOneBy([
                'code' => $code->getCode(),
                'codeSystem' => $code->getCodeSystem()
            ]);
            
            if ($existingCode === null) {
                $output->writeln(sprintf("Creating code with code '%s'", 
                    $code->getCode()), OutputInterface::VERBOSITY_NORMAL);
                $i++;
                $em->persist($code);
            }
            
            if ($i === 20) {
                $em->flush();
                // clear the em to avoid memory consumption
                $em->clear();
            }
        }
        
        // persist remaining codes
        $em->flush();
    }
    
    /**
     * 
     * @param InputInterface $input
     * @return type
     * @throws \InvalidArgumentException if the service is not found
     */
    private function getService(InputInterface $input, OutputInterface $output)
    {
        $service = $input->getArgument(self::SERVICE_NAME);
            
        try {
            return $this->getContainer()->get($service);
        } catch (ServiceNotFoundException $ex) {
            $output->writeln(sprintf("<error>The service %s is not "
                . "found</error>", $service));
            
            throw new \InvalidArgumentException(sprintf("The service %s is not "
                . "found", $service));
        }
    }

}
